import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/catpicture.dart';
import 'package:http/http.dart' as http;

Future<CatPicture> fetchCatPicture(String idrace) async {
  final response =
      await http.get(Uri.parse('https://api.thecatapi.com/v1/images/search?breed_id='+idrace+''));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    final jsonresponse = json.decode(response.body);
    return CatPicture.fromJson(jsonresponse);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}



class SecondRoute extends StatelessWidget {
  const SecondRoute({Key? key, required this.nomchat, required this.idrace}) : super(key: key);
  final String nomchat;
  final String idrace;
  get futureAlbum => fetchCatPicture(idrace);

 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.nomchat),
         
      ),
       body: Center(
          child: FutureBuilder<CatPicture>(
            future: futureAlbum,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Card(
      elevation: 4,
      margin: EdgeInsets.fromLTRB(0.0, 4.0, 4.0, 4.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      shadowColor: Colors.white,
      color: Colors.white70,
      child: Stack(children: <Widget>[
        Image(
            image: NetworkImage(snapshot.data!.url[0][0]),
          ),
        
        Column(mainAxisAlignment: MainAxisAlignment.end, children: [
          Container(
              width: double.infinity,
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(80.0, 80.0, 80.0, 80.0),
              child: Text(
                snapshot.data!.description[0][0],
                maxLines: 10,
                textAlign: TextAlign.center,
              )),
        ]),
      ]),
    );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }

              // By default, show a loading spinner.
              return const CircularProgressIndicator();
            },
          ),
        ),
    );
  }
}