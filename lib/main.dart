import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/ui/screens/SecondRoute.dart';
import 'package:http/http.dart' as http;
import 'models/catraceliste.dart';


Future<CatRaceListe> fetchCatBreed() async {
  final response =
      await http.get(Uri.parse('https://api.thecatapi.com/v1/breeds'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    final jsonresponse = json.decode(response.body);
    return CatRaceListe.fromJson(jsonresponse);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}


void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future<CatRaceListe> futureAlbum;
  @override
  void initState() {
    super.initState();
    futureAlbum = fetchCatBreed();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Chat Race Liste'),
        ),
        body: Center(
          child: FutureBuilder<CatRaceListe>(
            future: futureAlbum,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data!.name[0].length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(snapshot.data!.name[0][index]),
                      leading: const Icon(Icons.pets),
                      trailing: const Icon(Icons.favorite_border),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => SecondRoute(nomchat: snapshot.data!.name[0][index], idrace: snapshot.data!.idrace[index])),
                        );
                      },
                    );
                  },
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }

              // By default, show a loading spinner.
              return const CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}


