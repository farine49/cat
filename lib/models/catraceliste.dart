class CatRaceListe {
  final List name;
  final List idrace;
  const CatRaceListe({
    required this.name,
     required this.idrace,
  });

  factory CatRaceListe.fromJson(List<dynamic> json) {
    final List<dynamic> cats = [];
    final List<dynamic> idrace = [];
    final List<dynamic> features = json;
    features.forEach((featureJson) {
      cats.add(featureJson["name"]);
      idrace.add(featureJson["id"]);
    });
    return CatRaceListe(name: [cats], idrace: idrace);
  }
}
