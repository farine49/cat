class CatPicture {
  final List url;
  final List description;
  const CatPicture({
    required this.url, required this.description,
  });

  factory CatPicture.fromJson(List<dynamic> json) {
    final List<dynamic> cats = [];
    final List<dynamic> features = json;
    final List<dynamic> catsdescription = [];
    features.forEach((featureJson) {
      cats.add(featureJson["url"]);
      catsdescription.add(featureJson["breeds"][0]["description"]);
    });
   
    return CatPicture(url: [cats],  description: [catsdescription]);
  }
}
